const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express();
const bodyParser = require("body-parser");
var cors = require("cors");
const ffmpegInstaller = require("@ffmpeg-installer/ffmpeg");
const ffmpeg = require("fluent-ffmpeg");
let multer = require("multer");
let upload = multer();
app.use(express.static("assets/images"));
ffmpeg.setFfmpegPath(ffmpegInstaller.path);
module.exports = ffmpeg;
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

const https = require("https");

var privateKey = fs.readFileSync("key.pem", "utf8");
var certificate = fs.readFileSync("cert.pem", "utf8");
console.log(certificate);
var credentials = { key: privateKey, cert: certificate };
app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});

app.post("/upload", upload.single("webmasterfile", "IdR"), (req, res) => {
  const file = req.file;
  const IDR = req.body.IdR;

  var imageBuffer = file.buffer.toString();
  let base64Image = imageBuffer.split(";base64,").pop(); //remove the base64 from blob as it doesn't work
  // console.log(imageBuffer);
  var imageName = "assets/" + IDR + ".webm";

  fs.writeFile(imageName, base64Image, { encoding: "base64" }, err => {
    if (err) throw err;

    var inFilename = imageName;
    var outFilename = "assets/" + IDR + ".mp4";
    ffmpeg(inFilename)
      .outputOptions("-c:v", "copy") // this will copy the data instead or reencode it
      .save(outFilename);

    console.log("The file has been saved!");

    var proc = new ffmpeg(imageName).takeScreenshots(
      {
        count: 1,
        timemarks: ["1"] // number of seconds
      },
      "assets/images/" + IDR,
      function(err) {
        console.log("screenshots were saved");
      }
    );
  });

  res.status(200).send("ok");
});

app.get("/video", function(req, res) {
  let video = req.query.video;
  console.log(video);
  const path = "assets/" + video + ".mp4";
  const stat = fs.statSync(path);
  const fileSize = stat.size;
  const range = req.headers.range;

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-");
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

    const chunksize = end - start + 1;
    const file = fs.createReadStream(path, { start, end });
    const head = {
      "Content-Range": `bytes ${start}-${end}/${fileSize}`,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": "video/mp4"
    };

    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      "Content-Length": fileSize,
      "Content-Type": "video/mp4"
    };
    res.writeHead(200, head);
    fs.createReadStream(path).pipe(res);
  }
});

// app.listen(3000, function() {
//   console.log("App is running on port 3000");
// });

https
  .createServer(
    {
      key: fs.readFileSync("/etc/letsencrypt/live/plain.red/privkey.pem"),
      cert: fs.readFileSync("/etc/letsencrypt/live/plain.red/cert.pem")
    },
    app
  )
  .listen(3000, function() {
    console.log(
      "Example app listening on port 3000! Go to https://localhost:3000/"
    );
  });
