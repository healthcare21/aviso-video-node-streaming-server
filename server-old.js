const express = require("express");
const fs = require("fs");
const path = require("path");
const app = express();
const bodyParser = require("body-parser");
var cors = require("cors");
const ffmpegInstaller = require("@ffmpeg-installer/ffmpeg");
const ffmpeg = require("fluent-ffmpeg");
let multer = require("multer");
let upload = multer();
ffmpeg.setFfmpegPath(ffmpegInstaller.path);
module.exports = ffmpeg;
app.use(cors());
app.use(express.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

app.get("/", function(req, res) {
  res.sendFile(path.join(__dirname + "/index.html"));
});

app.post("/upload", upload.single("webmasterfile"), (req, res) => {
  const file = req.file;
  console.log(file.buffer);
  var imageBuffer = file.buffer;
  // console.log(imageBuffer);
  var imageName = "assets/test.mp4";
  var imageNameP = "assets/test2.webm";
  // var imageBuffer = decodeBase64Image(imageBuffer);
  //var data = imageBuffer.replace(/^data:image\/\w+;base64,/, "");
  // var buf = new Buffer(imageBuffer, "base64");
  // fs.writeFile('image.png', buf);

  fs.writeFile(imageName, imageBuffer, { encoding: "base64" }, err => {
    if (err) throw err;
    console.log("The file has been saved!");
    fs.readFile(imageName, "utf8", function read(err, data) {
      let base64Image = data.split(";base64,").pop();
      fs.writeFile(imageNameP, base64Image, { encoding: "base64" }, err => {
        if (err) throw err;

        console.log("The file has been saved!");
      });
    });
  });

  // fs.createWriteStream(imageName, [(encoding = "MPEG-4")]).write(imageBuffer);
  // let formData = req.body;
  // console.log("Data", formData);
  res.status(200).send("ok");
});

app.get("/video", function(req, res) {
  var inFilename = "/assets/test2.webm";
  var outFilename = "/assets/video.mp4";
  ffmpeg(inFilename)
    .outputOptions("-c:v", "copy") // this will copy the data instead or reencode it
    .save(outFilename);
  let video = req.query.video;
  const path = "assets/sample.mp4";
  const stat = fs.statSync(path);
  const fileSize = stat.size;
  const range = req.headers.range;

  if (range) {
    const parts = range.replace(/bytes=/, "").split("-");
    const start = parseInt(parts[0], 10);
    const end = parts[1] ? parseInt(parts[1], 10) : fileSize - 1;

    const chunksize = end - start + 1;
    const file = fs.createReadStream(path, { start, end });
    const head = {
      "Content-Range": `bytes ${start}-${end}/${fileSize}`,
      "Accept-Ranges": "bytes",
      "Content-Length": chunksize,
      "Content-Type": "video/mp4"
    };

    res.writeHead(206, head);
    file.pipe(res);
  } else {
    const head = {
      "Content-Length": fileSize,
      "Content-Type": "video/mp4"
    };
    res.writeHead(200, head);
    fs.createReadStream(path).pipe(res);
  }
});

app.listen(3000, function() {
  console.log("App is running on port 3000");
});
